package repository

import (
	"demo/entity"

	"golang.org/x/net/context"
	"gorm.io/gorm"
)

type UserRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) *UserRepository {
	return &UserRepository{db: db}
}

func (repo *UserRepository) RegisterUser(ctx context.Context, data *entity.User) error {
	if err := repo.db.Create(&data).Error; err != nil {
		return err
	}
	return nil
}
func (repo *UserRepository) UserLogIn(ctx context.Context, data *entity.User) error {
	if err := repo.db.First(&data, "gmail = ?", data.Gmail).Error; err != nil {
		return err
	}
	return nil
}
func (repo *UserRepository) GetUserData(ctx context.Context, data *entity.User) error {
	if err := repo.db.Preload("Shop").First(&data, "id = ?", data.ID).Error; err != nil {
		return err
	}
	return nil
}
func (repo *UserRepository) ChangePassword(ctx context.Context, data *entity.User) error {
	if err := repo.db.Model(&data).Where("id=?", data.ID).Updates(&data).Error; err != nil {
		return err
	}
	return nil
}
