package repository

import (
	"demo/business"
	"demo/entity"

	"golang.org/x/net/context"
	"gorm.io/gorm"
)

type ProductRepository struct {
	db *gorm.DB
}

func NewProductRepository(db *gorm.DB) *ProductRepository {
	return &ProductRepository{db: db}
}

func (repo *ProductRepository) AddProduct(ctx context.Context, data *entity.Product) error {
	if err := repo.db.Create(&data).Error; err != nil {
		return err
	}
	return nil
}

func (repo *ProductRepository) GetProductById(ctx context.Context, data *entity.Product) error {
	if err := repo.db.Preload("Shop.User").Where("id=?", data.ID).First(&data).Error; err != nil {
		return err
	}
	return nil
}
func (repo *ProductRepository) UpdateProduct(ctx context.Context, data *entity.Product) error {
	if err := repo.db.Preload("Shop.User").Where("id=?", data.ID).Updates(&data).Error; err != nil {
		return err
	}
	return nil
}
func (repo *ProductRepository) DeleteProduct(ctx context.Context, data *entity.Product) error {
	if err := repo.db.Model(&data).Where("id=? AND status=?", data.ID, "Active").Update("status", "Inactive").Error; err != nil {
		return err
	}
	return nil
}
func (repo *ProductRepository) RecoverProduct(ctx context.Context, data *entity.Product) error {
	if err := repo.db.Model(&data).Where("id=? AND status=?", data.ID, "Inactive").Update("status", "Active").Error; err != nil {
		return err
	}
	return nil
}
func (repo *ProductRepository) ListProduct(ctx context.Context, paging *business.Paging, data *[]entity.Product) error {
	repo.db = repo.db.Preload("Shop.User").Model(&data).Where("status <> ?", "Inactive")

	if err := repo.db.Preload("Shop.User").Model(&data).
		Count(&paging.Total).Error; err != nil {
		return err
	}

	if err := repo.db.Order("id desc").
		Offset((paging.Page - 1) * paging.Limit).
		Limit(paging.Limit).
		Find(&data).Error; err != nil {
		return err
	}
	return nil
}

func (repo *ProductRepository) ListShopProduct(ctx context.Context, paging *business.Paging, data *[]entity.Product, shop *entity.Shop) error {
	repo.db = repo.db.Model(&data).Preload("Shop.User").Where("status <> ? AND shop_id = ?", "Inactive", shop.ID)

	if err := repo.db.Model(&data).Preload("Shop.User").
		Count(&paging.Total).Error; err != nil {
		return err
	}

	if err := repo.db.Order("id desc").
		Offset((paging.Page - 1) * paging.Limit).
		Limit(paging.Limit).
		Find(&data).Error; err != nil {
		return err
	}
	return nil
}

func (repo *ProductRepository) GetAuthorizedProduct(ctx context.Context, data *entity.Product) error {
	if err := repo.db.Model(&data).Where("id =? AND shop_id=?", data.ID, data.ShopId).First(&data).Error; err != nil {
		return err
	}
	return nil
}

func (repo *ProductRepository) SearchProduct(ctx context.Context, data *[]entity.Product, search string) error {
	if err := repo.db.Model(&data).Preload("Shop.User").Where("product_name = ?", "%"+search+"%").Find(&data).Error; err != nil {
		return err
	}
	return nil
}
