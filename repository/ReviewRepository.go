package repository

import (
	"demo/entity"

	"golang.org/x/net/context"
	"gorm.io/gorm"
)

type ReviewRepository struct {
	db *gorm.DB
}

func NewReviewRepository(db *gorm.DB) *ReviewRepository {
	return &ReviewRepository{db: db}
}

func (repo *ReviewRepository) WriteReview(ctx context.Context, data *entity.Review) error {
	if err := repo.db.Create(&data).Error; err != nil {
		return err
	}
	return nil
}

func (repo *ReviewRepository) ViewProductReviews(ctx context.Context, data *[]entity.Review, product *entity.Product) error {
	if err := repo.db.Model(&data).Preload("User").Preload("Product").Where("product_id=?", product.ID).Find(&data).Error; err != nil {
		return err
	}
	return nil
}
