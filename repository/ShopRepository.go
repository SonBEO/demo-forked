package repository

import (
	"demo/entity"

	"golang.org/x/net/context"
	"gorm.io/gorm"
)

type ShopRepository struct {
	db *gorm.DB
}

func NewShopRepository(db *gorm.DB) *ShopRepository {
	return &ShopRepository{db: db}
}

func (repo *ShopRepository) AccessShop(ctx context.Context, data *entity.Shop) error {
	if err := repo.db.Preload("User").Model(&data).Where("user_id=?", data.UserId).First(&data).Error; err != nil {
		return err
	}
	return nil
}

func (repo *ShopRepository) CreateShop(ctx context.Context, data *entity.Shop) error {
	if err := repo.db.Create(&data).Error; err != nil {
		return err
	}
	return nil
}
