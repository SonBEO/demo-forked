package repository

import (
	"demo/entity"

	"golang.org/x/net/context"
	"gorm.io/gorm"
)

type CartRepository struct {
	db *gorm.DB
}

func NewCartRepository(db *gorm.DB) *CartRepository {
	return &CartRepository{db: db}
}

func (repo *CartRepository) GetCart(ctx context.Context, data *entity.Cart) error {
	if err := repo.db.Model(&data).Where("user_id = ?", data.UserId).FirstOrCreate(&data).Error; err != nil {
		return err
	}
	return nil
}

func (repo *CartRepository) AddNewProductToCart(ctx context.Context, data *entity.CartDetail) error {
	if err := repo.db.Model(&data).Create(&data).Error; err != nil {
		return err
	}
	return nil
}

func (repo *CartRepository) ViewCartDetail(ctx context.Context, data *[]entity.CartDetail, cart *entity.Cart) error {
	if err := repo.db.Model(&data).Preload("Cart.User").Find(&data).Where("cart_id = ? AND status = ?", cart.ID, "1").Error; err != nil {
		return err
	}
	return nil
}

func (repo *CartRepository) UpdateCartDetail(ctx context.Context, data *entity.Cart) error {
	if err := repo.db.Model(&data).Updates(&data).Where("user_id=?", data.UserId).Error; err != err {
		return err
	}
	return nil
}
