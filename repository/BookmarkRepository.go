package repository

import (
	"demo/entity"

	"golang.org/x/net/context"
	"gorm.io/gorm"
)

type BookmarkRepository struct {
	db *gorm.DB
}

func NewBookmarkRepository(db *gorm.DB) *BookmarkRepository {
	return &BookmarkRepository{db: db}
}

func (repo *BookmarkRepository) AddProductBookmark(ctx context.Context, data *entity.BookmarkedProduct) error {
	if err := repo.db.Model(&data).Where("user_id=? AND product_id =?", data.UserId, data.ProductId).First(&data).Error; err != nil {
		if err := repo.db.Create(&data).Error; err != nil {
			return err
		}
		return nil

	}

	data.Status = "Active"
	if err := repo.db.Save(&data).Error; err != nil {
		return err
	}
	return nil
}

func (repo *BookmarkRepository) AddShopBookmark(ctx context.Context, data *entity.BookmarkedShop) error {
	if err := repo.db.Model(&data).Where("user_id=? AND product_id =?", data.UserId, data.ShopId).First(&data).Error; err != nil {
		if err := repo.db.Create(&data).Error; err != nil {
			return err
		}
		return nil
	}
	data.Status = "Active"
	if err := repo.db.Save(&data).Error; err != nil {
		return err
	}
	return nil
}

func (repo *BookmarkRepository) ShowProductBookmark(ctx context.Context, data *[]entity.BookmarkedProduct, userdata *entity.User) error {
	if err := repo.db.Model(&data).Where("user_id=? and status <> ?", userdata.ID, "Inactive").Find(&data).Error; err != nil {
		return err
	}
	return nil
}
func (repo *BookmarkRepository) ShowShopBookmark(ctx context.Context, data *[]entity.BookmarkedShop, userdata *entity.User) error {
	if err := repo.db.Model(&data).Where("user_id=? and status <> ?", userdata.ID, "Inactive").Find(&data).Error; err != nil {
		return err
	}
	return nil
}

func (repo *BookmarkRepository) RemoveProductBookmark(ctx context.Context, data *entity.BookmarkedProduct) error {
	if err := repo.db.Model(&data).Updates(&data).Where("user_id = ? and product_id=? ", data.UserId, data.ProductId).Error; err != nil {
		return err
	}
	return nil
}

func (repo *BookmarkRepository) RemoveShopBookmark(ctx context.Context, data *entity.BookmarkedShop) error {
	if err := repo.db.Model(&data).Updates(&data).Where("user_id = ? and product_id=? ", data.UserId, data.ShopId).Error; err != nil {
		return err
	}
	return nil
}
