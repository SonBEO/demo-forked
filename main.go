package main

import (
	"demo/controller"
	"demo/initializer"

	"github.com/gin-gonic/gin"
)

func main() {
	initializer.LoadEnvVariables()
	db := initializer.DBConnect()

	r := gin.Default()
	controller.SetupRouter(r, db)
	r.Run()
}
