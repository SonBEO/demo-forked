package business

import (
	"errors"
	"math/rand"
	"time"
)

var (
	ErrEmailAlreadyExists  = errors.New("email is registered")
	ErrOwnerAlreadyOwnShop = errors.New("This user owned a shop.")
)

// Paging
type Paging struct {
	Page  int   `json:"page" form:"page"`
	Limit int   `json:"limit" form:"limit"`
	Total int64 `json:"total" form:"-"`
}

func (p *Paging) CreateLimit() {
	if p.Page <= 0 {
		p.Page = 1
	}
	p.Limit = 5
}

// Create new random password
const charset = "abcdefghijklmnopqrstuvwxyz" +
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

var seededRand *rand.Rand = rand.New(
	rand.NewSource(time.Now().UnixNano()))

func StringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

func NewPassword(length int) string {
	return StringWithCharset(length, charset)
}
