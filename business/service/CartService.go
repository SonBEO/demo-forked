package service

import (
	"context"
	"demo/entity"
)

type CartRepository interface {
	GetCart(ctx context.Context, data *entity.Cart) error
	AddNewProductToCart(ctx context.Context, data *entity.CartDetail) error
	ViewCartDetail(ctx context.Context, data *[]entity.CartDetail, cart *entity.Cart) error
	UpdateCartDetail(ctx context.Context, data *entity.Cart) error
}

type CartService struct {
	repo CartRepository
}

func NewCartService(repo CartRepository) *CartService {
	return &CartService{repo: repo}
}

func (service *CartService) GetCart(ctx context.Context, data *entity.Cart) error {
	if err := service.repo.GetCart(ctx, data); err != nil {
		return err
	}
	return nil
}

func (service *CartService) AddNewProductToCart(ctx context.Context, data *entity.CartDetail) error {
	if err := service.repo.AddNewProductToCart(ctx, data); err != nil {
		return err
	}
	return nil
}

func (service *CartService) ViewCartDetail(ctx context.Context, data *[]entity.CartDetail, cart *entity.Cart) error {
	if err := service.repo.ViewCartDetail(ctx, data, cart); err != nil {
		return err
	}
	return nil
}

func (service *CartService) UpdateCartDetail(ctx context.Context, data *entity.Cart) error {
	if err := service.repo.UpdateCartDetail(ctx, data); err != nil {
		return err
	}
	return nil
}
