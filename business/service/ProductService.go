package service

import (
	"context"
	"demo/business"
	"demo/entity"
)

type ProductRepository interface {
	AddProduct(ctx context.Context, data *entity.Product) error
	GetProductById(ctx context.Context, data *entity.Product) error
	UpdateProduct(ctx context.Context, data *entity.Product) error
	DeleteProduct(ctx context.Context, data *entity.Product) error
	RecoverProduct(ctx context.Context, data *entity.Product) error
	ListProduct(ctx context.Context, paging *business.Paging, data *[]entity.Product) error
	ListShopProduct(ctx context.Context, paging *business.Paging, data *[]entity.Product, shop *entity.Shop) error
	GetAuthorizedProduct(ctx context.Context, data *entity.Product) error
	SearchProduct(ctx context.Context, data *[]entity.Product, search string) error
}

type ProductService struct {
	repo ProductRepository
}

func NewProductService(repo ProductRepository) *ProductService {
	return &ProductService{repo: repo}
}

func (service *ProductService) AddProduct(ctx context.Context, data *entity.Product) error {
	if err := service.repo.AddProduct(ctx, data); err != nil {
		return err
	}
	return nil
}
func (service *ProductService) GetProductById(ctx context.Context, data *entity.Product) error {
	if err := service.repo.GetProductById(ctx, data); err != nil {
		return err
	}
	return nil
}
func (service *ProductService) UpdateProduct(ctx context.Context, data *entity.Product) error {
	if err := service.repo.UpdateProduct(ctx, data); err != nil {
		return err
	}
	return nil
}
func (service *ProductService) DeleteProduct(ctx context.Context, data *entity.Product) error {
	if err := service.repo.DeleteProduct(ctx, data); err != nil {
		return err
	}
	return nil
}

func (service *ProductService) RecoverProduct(ctx context.Context, data *entity.Product) error {
	if err := service.repo.RecoverProduct(ctx, data); err != nil {
		return err
	}
	return nil
}

func (service *ProductService) ListProduct(ctx context.Context, paging *business.Paging, data *[]entity.Product) error {
	if err := service.repo.ListProduct(ctx, paging, data); err != nil {
		return err
	}
	return nil
}
func (service *ProductService) ListShopProduct(ctx context.Context, paging *business.Paging, data *[]entity.Product, shop *entity.Shop) error {
	if err := service.repo.ListShopProduct(ctx, paging, data, shop); err != nil {
		return err
	}
	return nil
}
func (service *ProductService) GetAuthorizedProduct(ctx context.Context, data *entity.Product) error {
	if err := service.repo.GetAuthorizedProduct(ctx, data); err != nil {
		return err
	}
	return nil
}

func (service *ProductService) SearchProduct(ctx context.Context, data *[]entity.Product, search string) error {
	if err := service.repo.SearchProduct(ctx, data, search); err != nil {
		return err
	}
	return nil
}
