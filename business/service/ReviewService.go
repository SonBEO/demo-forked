package service

import (
	"context"
	"demo/entity"
)

type ReviewRepository interface {
	WriteReview(ctx context.Context, data *entity.Review) error
	ViewProductReviews(ctx context.Context, data *[]entity.Review, product *entity.Product) error
}
type ReviewService struct {
	repo ReviewRepository
}

func NewReviewService(repo ReviewRepository) *ReviewService {
	return &ReviewService{repo: repo}
}
func (service *ReviewService) WriteReview(ctx context.Context, data *entity.Review) error {
	if err := service.repo.WriteReview(ctx, data); err != nil {
		return err
	}
	return nil
}
func (service *ReviewService) ViewProductReviews(ctx context.Context, data *[]entity.Review, product *entity.Product) error {
	if err := service.repo.ViewProductReviews(ctx, data, product); err != nil {
		return err
	}
	return nil
}
