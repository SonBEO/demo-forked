package service

import (
	"context"
	"demo/entity"
)

type ShopRepository interface {
	AccessShop(ctx context.Context, data *entity.Shop) error
	CreateShop(ctx context.Context, data *entity.Shop) error
}

type ShopService struct {
	repo ShopRepository
}

func NewShopService(repo ShopRepository) *ShopService {
	return &ShopService{repo: repo}
}

func (service *ShopService) AccessShop(ctx context.Context, data *entity.Shop) error {
	if err := service.repo.AccessShop(ctx, data); err != nil {
		return err
	}
	return nil
}

func (service *ShopService) CreateShop(ctx context.Context, data *entity.Shop) error {
	if err := service.repo.CreateShop(ctx, data); err != nil {
		return err
	}
	return nil
}
