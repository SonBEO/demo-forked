package service

import (
	"context"
	"demo/entity"
)

type UserRepository interface {
	RegisterUser(ctx context.Context, data *entity.User) error
	UserLogIn(ctx context.Context, data *entity.User) error
	GetUserData(ctx context.Context, data *entity.User) error
	ChangePassword(ctx context.Context, data *entity.User) error
}

type UserService struct {
	repo UserRepository
}

func NewUserService(repo UserRepository) *UserService {
	return &UserService{repo: repo}
}

func (service *UserService) RegisterUser(ctx context.Context, data *entity.User) error {
	if err := service.repo.RegisterUser(ctx, data); err != nil {
		return err
	}
	return nil
}
func (service *UserService) UserLogIn(ctx context.Context, data *entity.User) error {
	if err := service.repo.UserLogIn(ctx, data); err != nil {
		return err
	}
	return nil
}
func (service *UserService) GetUserData(ctx context.Context, data *entity.User) error {
	if err := service.repo.GetUserData(ctx, data); err != nil {
		return err
	}
	return nil
}

func (service *UserService) ChangePassword(ctx context.Context, data *entity.User) error {
	if err := service.repo.ChangePassword(ctx, data); err != nil {
		return err
	}
	return nil
}
