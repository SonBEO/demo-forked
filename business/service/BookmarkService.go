package service

import (
	"context"
	"demo/entity"
)

type BookmarkRepository interface {
	AddShopBookmark(ctx context.Context, data *entity.BookmarkedShop) error
	AddProductBookmark(ctx context.Context, data *entity.BookmarkedProduct) error
	ShowProductBookmark(ctx context.Context, data *[]entity.BookmarkedProduct, userdata *entity.User) error
	ShowShopBookmark(ctx context.Context, data *[]entity.BookmarkedShop, userdata *entity.User) error
	RemoveProductBookmark(ctx context.Context, data *entity.BookmarkedProduct) error
	RemoveShopBookmark(ctx context.Context, data *entity.BookmarkedShop) error
}

type BookmarkService struct {
	repo BookmarkRepository
}

func NewBookmarkService(repo BookmarkRepository) *BookmarkService {
	return &BookmarkService{repo: repo}
}

func (service *BookmarkService) AddProductBookmark(ctx context.Context, data *entity.BookmarkedProduct) error {
	if err := service.repo.AddProductBookmark(ctx, data); err != nil {
		return err
	}
	return nil
}

func (service *BookmarkService) AddShopBookmark(ctx context.Context, data *entity.BookmarkedShop) error {
	if err := service.repo.AddShopBookmark(ctx, data); err != nil {
		return err
	}
	return nil
}

func (service *BookmarkService) ShowProductBookmark(ctx context.Context, data *[]entity.BookmarkedProduct, userdata *entity.User) error {
	if err := service.repo.ShowProductBookmark(ctx, data, userdata); err != nil {
		return err
	}
	return nil
}

func (service *BookmarkService) ShowShopBookmark(ctx context.Context, data *[]entity.BookmarkedShop, userdata *entity.User) error {
	if err := service.repo.ShowShopBookmark(ctx, data, userdata); err != nil {
		return err
	}
	return nil
}

func (service *BookmarkService) RemoveProductBookmark(ctx context.Context, data *entity.BookmarkedProduct) error {
	if err := service.repo.RemoveProductBookmark(ctx, data); err != nil {
		return err
	}
	return nil
}

func (service *BookmarkService) RemoveShopBookmark(ctx context.Context, data *entity.BookmarkedShop) error {
	if err := service.repo.RemoveShopBookmark(ctx, data); err != nil {
		return err
	}
	return nil
}
