package controller

import (
	"demo/business"
	"demo/business/service"
	"demo/entity"
	"demo/repository"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func AddProduct(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data entity.Product
		user, _ := ctx.Get("userdata")
		if user == nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		var ReqBody struct {
			ReqProductName        string
			ReqProductDescription string
			ReqPrice              float64
			ReqImage              string
		}
		file, err := ctx.FormFile("image")
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		NewFileName := time.Now().Format("20060102150405") + ".jpg"

		uploadDir := "images/"
		if _, err := os.Stat(uploadDir); os.IsNotExist(err) {
			os.Mkdir(uploadDir, os.ModePerm)
		}

		newFilePath := uploadDir + NewFileName
		if err := ctx.SaveUploadedFile(file, newFilePath); err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to save file"})
			return
		}

		newFilePath = "http://localhost:8080/" + newFilePath
		ReqBody.ReqProductName = ctx.PostForm("product_name")
		ReqBody.ReqProductDescription = ctx.PostForm("product_desc")
		ReqBody.ReqPrice, err = strconv.ParseFloat(ctx.PostForm("price"), 64)
		if err != nil {
			err.Error()
		}
		ReqBody.ReqImage = newFilePath

		if err := ctx.ShouldBind(&ReqBody); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		data.ProductName = ReqBody.ReqProductName
		data.ProductDescription = ReqBody.ReqProductDescription
		data.Price = ReqBody.ReqPrice
		data.Image = ReqBody.ReqImage
		data.Status = "Active"
		data.ShopId = user.(entity.User).Shop.ID
		repository := repository.NewProductRepository(db)
		service := service.NewProductService(repository)
		if err := service.AddProduct(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"msg": "Cannot add new product.",
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"msg": "New Product Added!!",
		})
	}
}

func GetProductById(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		id, err := strconv.Atoi(ctx.Param("id"))

		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		repository := repository.NewProductRepository(db)
		service := service.NewProductService(repository)
		var data entity.Product
		data.ID = uint(id)
		if err := service.GetProductById(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"msg": err.Error(),
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"ProductName": data.ProductName,
			"Shop":        data.Shop.ShopName,
			"Ownner":      data.Shop.User.UserName,
			"Description": data.ProductDescription,
			"Price":       data.Price,
			"Image":       data.Image,
		})
	}
}

func UpdateProduct(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		user, _ := ctx.Get("userdata")
		if user == nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		var data entity.Product
		id, err := strconv.Atoi(ctx.Param("id"))

		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		repository := repository.NewProductRepository(db)
		service := service.NewProductService(repository)
		data.ShopId = user.(entity.User).Shop.ID
		data.ID = uint(id)
		if err := service.GetAuthorizedProduct(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"err": "Unauthorized Product",
			})
		}
		var ReqBody struct {
			ReqProductName        string
			ReqProductDescription string
			ReqPrice              float64
		}
		ReqBody.ReqProductName = ctx.PostForm("product_name")
		ReqBody.ReqProductDescription = ctx.PostForm("product_desc")
		ReqBody.ReqPrice, err = strconv.ParseFloat(ctx.PostForm("price"), 64)
		if err != nil {
			err.Error()
		}
		if err := ctx.ShouldBind(&ReqBody); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		data.ProductName = ReqBody.ReqProductName
		data.ProductDescription = ReqBody.ReqProductDescription
		data.Price = ReqBody.ReqPrice
		if err := service.UpdateProduct(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"msg": "Cannot update product.",
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"msg": "Update OK",
		})
	}
}

func UpdateProductImage(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data entity.Product
		user, _ := ctx.Get("userdata")
		if user == nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		id, err := strconv.Atoi(ctx.Param("id"))
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		var ReqBody struct {
			ReqImage string
		}
		data.ID = uint(id)
		data.ShopId = user.(entity.User).Shop.ID
		repository := repository.NewProductRepository(db)
		service := service.NewProductService(repository)
		if err := service.GetAuthorizedProduct(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"err": "Unauthorized Product",
			})
		}
		ReqBody.ReqImage = data.Image
		ReqBody.ReqImage = strings.Replace(ReqBody.ReqImage, "http://localhost:8080/", "", -1)
		err = os.Remove(ReqBody.ReqImage)
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
			return
		}
		file, err := ctx.FormFile("image")
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		NewFileName := time.Now().Format("20060102150405") + ".jpg"

		uploadDir := "images/"
		if _, err := os.Stat(uploadDir); os.IsNotExist(err) {
			os.Mkdir(uploadDir, os.ModePerm)
		}

		newFilePath := uploadDir + NewFileName
		if err := ctx.SaveUploadedFile(file, newFilePath); err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to save file"})
			return
		}

		newFilePath = "http://localhost:8080/" + newFilePath
		ReqBody.ReqImage = newFilePath
		if err := ctx.ShouldBind(&ReqBody); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		data.Image = ReqBody.ReqImage
		if err := service.UpdateProduct(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": "Cannot update image",
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"msg": "Update OK",
		})
	}
}

func DeleteProduct(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data entity.Product
		user, _ := ctx.Get("userdata")
		if user == nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		id, err := strconv.Atoi(ctx.Param("id"))
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		var ReqBody struct {
			ReqId uint
		}
		ReqBody.ReqId = uint(id)
		data.ID = ReqBody.ReqId
		data.ShopId = user.(entity.User).Shop.ID
		repository := repository.NewProductRepository(db)
		service := service.NewProductService(repository)
		if err := service.GetAuthorizedProduct(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"err": "Unauthorized Product",
			})
		}
		if err := service.DeleteProduct(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": "Cannot delete item.",
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"msg": "Update OK",
		})
	}
}

func RecoverProduct(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data entity.Product
		user, _ := ctx.Get("userdata")
		if user == nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		id, err := strconv.Atoi(ctx.Param("id"))
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		var ReqBody struct {
			ReqId uint
		}
		ReqBody.ReqId = uint(id)
		data.ID = ReqBody.ReqId
		data.ShopId = user.(entity.User).Shop.ID
		repository := repository.NewProductRepository(db)
		service := service.NewProductService(repository)
		if err := service.GetAuthorizedProduct(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"err": "Unauthorized Product",
			})
		}
		if err := service.RecoverProduct(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": "Cannot recover item.",
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"msg": "Update OK",
		})
	}
}

func ListProduct(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var paging business.Paging

		if err := ctx.ShouldBind(&paging); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}

		paging.CreateLimit()

		var data []entity.Product

		repository := repository.NewProductRepository(db)
		service := service.NewProductService(repository)

		if err := service.ListProduct(ctx.Request.Context(), &paging, &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"data": data,
			"page": paging.Page,
		})
	}
}

func ListShopProduct(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var paging business.Paging

		if err := ctx.ShouldBind(&paging); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}

		id, err := strconv.Atoi(ctx.Param("id"))
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}

		paging.CreateLimit()

		var data []entity.Product
		var shop entity.Shop
		shop.ID = uint(id)
		repository := repository.NewProductRepository(db)
		service := service.NewProductService(repository)
		if err := service.ListShopProduct(ctx.Request.Context(), &paging, &data, &shop); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"data": data,
			"page": paging.Page,
		})
	}
}

func SearchProduct(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data []entity.Product
		var ReqSearch = ctx.PostForm("search")
		repository := repository.NewProductRepository(db)
		service := service.NewProductService(repository)
		if err := service.SearchProduct(ctx.Request.Context(), &data, ReqSearch); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"data": data,
		})
	}
}
