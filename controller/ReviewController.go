package controller

// User write a review on a product, validate rating

// Display all my review on all products

// Display all review of 1 product

import (
	"demo/business/service"
	"demo/entity"
	"demo/repository"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func WriteReview(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data entity.Review
		user, _ := ctx.Get("userdata")
		if user == nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		id, err := strconv.Atoi(ctx.Param("id"))

		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}

		var ReqBody struct {
			ReqRating    int
			ReqText      string
			ReqUserId    uint
			ReqProductId uint
		}
		ReqBody.ReqRating, err = strconv.Atoi(ctx.PostForm("rating"))
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": "Invalid Rating.",
			})
			return
		}
		ReqBody.ReqText = ctx.PostForm("text")
		ReqBody.ReqProductId = uint(id)
		ReqBody.ReqUserId = user.(entity.User).ID
		if ReqBody.ReqRating <= 0 && ReqBody.ReqRating > 5 {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": "Invalid Rating.",
			})
			return
		}

		data.ProductId = ReqBody.ReqProductId
		data.UserId = ReqBody.ReqUserId
		data.Rating = ReqBody.ReqRating
		data.Review = ReqBody.ReqText

		if err := ctx.ShouldBind(&ReqBody); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}

		repository := repository.NewReviewRepository(db)
		service := service.NewReviewService(repository)

		if err := service.WriteReview(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": "Couldn't write review.",
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"msg": "Review added",
		})
	}
}

func ViewProductReviews(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data []entity.Review
		id, err := strconv.Atoi(ctx.Param("id"))

		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}

		repository := repository.NewReviewRepository(db)
		service := service.NewReviewService(repository)

		var product entity.Product
		product.ID = uint(id)
		if err := service.ViewProductReviews(ctx.Request.Context(), &data, &product); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"err": "Cannot retrieve product reviews",
			})
		}
		ctx.JSON(http.StatusOK, gin.H{
			"reviews": data,
		})
	}
}
