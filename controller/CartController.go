package controller

import (
	"demo/business/service"
	"demo/entity"
	"demo/repository"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func GetCart(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data entity.Cart
		user, _ := ctx.Get("userdata")
		if user == nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		data.UserId = user.(entity.User).ID
		data.Status = "Active"
		repository := repository.NewCartRepository(db)
		service := service.NewCartService(repository)
		if err := service.GetCart(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"msg": "Cannot load Cart",
			})
		}
		ctx.JSON(http.StatusOK, gin.H{
			"CartID": data.ID,
		})
	}
}

func AddNewProductToCart(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data entity.CartDetail
		user, _ := ctx.Get("userdata")
		if user == nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		id, err := strconv.Atoi(ctx.Param("id"))

		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		var ReqBody struct {
			ReqProductId uint
			ReqCartId    uint
			ReqQuantity  int
		}
		ReqBody.ReqProductId = uint(id)

		var cartData entity.Cart
		cartData.UserId = user.(entity.User).ID
		cartData.Status = "Active"

		CartRepository := repository.NewCartRepository(db)
		CartService := service.NewCartService(CartRepository)

		if err := CartService.GetCart(ctx.Request.Context(), &cartData); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"msg": "Cannot load Cart",
			})
			return
		}
		ReqBody.ReqCartId = cartData.ID
		quantity, err := strconv.Atoi(ctx.PostForm("quantity"))
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": "Inccorrect Quantity Number.",
			})
			return
		}
		ReqBody.ReqQuantity = int(quantity)
		if err := ctx.ShouldBind(&ReqBody); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}

		data.ProductId = ReqBody.ReqProductId
		data.CartId = ReqBody.ReqCartId
		data.Quantity = ReqBody.ReqQuantity

		var productData entity.Product
		productData.ID = uint(id)
		ProductRepository := repository.NewProductRepository(db)
		ProductSerive := service.NewProductService(ProductRepository)

		if err := ProductSerive.GetProductById(ctx, &productData); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"msg": "Cannot load product.",
			})
			return
		}

		data.Price = productData.Price * float64(data.Quantity)
		data.Cart.Status = "1"
		if err := CartService.AddNewProductToCart(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"msg": "Cannot add to cart.",
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"msg": "Product added to cart",
		})
	}
}

func ViewCartDetail(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data []entity.CartDetail
		user, _ := ctx.Get("userdata")
		if user == nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		var cart entity.Cart
		CartRepository := repository.NewCartRepository(db)
		CartService := service.NewCartService(CartRepository)

		if err := CartService.GetCart(ctx.Request.Context(), &cart); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"msg": "Cannot load Cart",
			})
			return
		}

		if err := CartService.ViewCartDetail(ctx.Request.Context(), &data, &cart); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"msg": "Cannot load Cart Detail",
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"data": data,
		})
	}
}

func UpdateCartDetail(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data entity.Cart
		user, _ := ctx.Get("userdata")
		if user == nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		var ReqBody struct {
			ReqPhoneNumber string
			ReqLocation    string
		}
		repository := repository.NewCartRepository(db)
		service := service.NewCartService(repository)
		ReqBody.ReqPhoneNumber = ctx.PostForm("phone-number")
		ReqBody.ReqLocation = ctx.PostForm("location")
		if err := service.UpdateCartDetail(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"msg": "Cannot Update Cart Detail",
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"msg": "Detail Updated",
		})
	}
}
