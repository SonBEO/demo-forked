package controller

import (
	"demo/entity"
	"demo/middleware"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func SetupRouter(r *gin.Engine, db *gorm.DB) {

	r.LoadHTMLGlob("templates/*")

	r.Static("/images", "./images")

	r.GET("/", middleware.ClaimsAuth, UserDataDisplay())

	r.GET("/bad-request", BadRequestScreen())

	User := r.Group("/user")
	{
		User.POST("/register", RegisterUser(db))
		User.GET("/register", GetRegisterUserForm())
		User.GET("/unauthorized", Get401Screen())
		User.GET("/login", GetLogInForm())
		User.POST("/login", LogIn(db))
		User.GET("/logout", LogOut())
		User.POST("/forgot-password", middleware.ClaimsAuth, ChangePassword(db))
	}
	Shop := r.Group("/shop")
	{
		Shop.GET("/access-shop", middleware.ClaimsAuth, AccessShop(db))
		Shop.POST("/create-shop", middleware.ClaimsAuth, CreateShop(db))
	}
	Product := r.Group("/product")
	{
		Product.GET("/get-product/:id", GetProductById(db))
		Product.POST("/add-product", middleware.ClaimsAuth, AddProduct(db))
		Product.PATCH("/update-product/:id", middleware.ClaimsAuth, UpdateProduct(db))
		Product.PATCH("/update-image/:id", middleware.ClaimsAuth, UpdateProductImage(db))
		Product.PATCH("/delete-product/:id", middleware.ClaimsAuth, DeleteProduct(db))
		Product.PATCH("/recover-product/:id", middleware.ClaimsAuth, RecoverProduct(db))
		Product.GET("/list-product", ListProduct(db))
		Product.GET("/shop-product/:id", ListShopProduct(db))
	}
	Bookmark := r.Group("/bookmark")
	{
		Bookmark.GET("/get-product-bookmark", middleware.ClaimsAuth, ShowProductBookmark(db))
		Bookmark.GET("/get-shop-bookmark", middleware.ClaimsAuth, ShowShopBookmark(db))
		Bookmark.POST("/add-product-bookmark/:id", middleware.ClaimsAuth, AddProductBookmark(db))
		Bookmark.POST("/add-shop-bookmark/:id", middleware.ClaimsAuth, AddShopBookmark(db))
		Bookmark.PATCH("/remove-product-bookmark/:id", middleware.ClaimsAuth, RemoveProductBookmark(db))
		Bookmark.PATCH("/remove-shop-bookmark/:id", middleware.ClaimsAuth, RemoveShopBookmark(db))
	}
	Review := r.Group("/review")
	{
		Review.GET("/get-reviews/:id", ViewProductReviews(db))
		Review.POST("/write-review/:id", middleware.ClaimsAuth, WriteReview(db))
	}
	Cart := r.Group("/cart")
	{
		Cart.GET("/get-cart", middleware.ClaimsAuth, GetCart(db))
		Cart.POST("/add-to-cart/:id", middleware.ClaimsAuth, AddNewProductToCart(db))
		Cart.GET("/cart-detail", middleware.ClaimsAuth, ViewCartDetail(db))
		Cart.PATCH("/cart-detail-update", middleware.ClaimsAuth, UpdateCartDetail(db))
	}
}

func UserDataDisplay() func(*gin.Context) {
	return func(ctx *gin.Context) {
		user, _ := ctx.Get("userdata")
		if user == nil {
			ctx.HTML(http.StatusUnauthorized, "LoggedOut.html", gin.H{})
		} else {
			ctx.JSON(http.StatusOK, gin.H{
				"Id":         user.(entity.User).ID,
				"Email":      user.(entity.User).Gmail,
				"UserName":   user.(entity.User).UserName,
				"Created_at": user.(entity.User).CreatedAt,
			})
		}
	}
}

func BadRequestScreen() func(*gin.Context) {
	return func(ctx *gin.Context) {
		ctx.HTML(http.StatusInternalServerError, "BadRequest.html", gin.H{})
	}
}
