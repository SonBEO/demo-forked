package controller

import (
	"crypto/tls"
	"demo/business"
	"demo/business/service"
	"demo/entity"
	"demo/repository"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v5"
	"golang.org/x/crypto/bcrypt"
	gomail "gopkg.in/mail.v2"
	"gorm.io/gorm"
)

func Get401Screen() func(*gin.Context) {
	return func(ctx *gin.Context) {
		var val string
		err := ctx.Query("error")
		if err == "token" {
			val = "Token Expired."
		}
		ctx.HTML(http.StatusInternalServerError, "Unauthorized.html", gin.H{
			"error": val,
		})
	}
}

func GetRegisterUserForm() func(*gin.Context) {
	return func(ctx *gin.Context) {
		var val string
		err := ctx.Query("error")
		if err == "user-registered" {
			val = "Email is registered."
		}
		ctx.HTML(http.StatusOK, "RegisterUserForm.html", gin.H{
			"err": val,
		})

	}
}

func RegisterUser(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data entity.User

		var ReqBody struct {
			ReqGmail    string
			ReqUserName string
			ReqPassword string
		}

		ReqBody.ReqGmail = ctx.PostForm("email")
		ReqBody.ReqUserName = ctx.PostForm("username")
		ReqBody.ReqPassword = ctx.PostForm("password")

		if err := ctx.ShouldBind(&ReqBody); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}

		hash, err := bcrypt.GenerateFromPassword([]byte(ReqBody.ReqPassword), 10)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}

		data.Gmail = ReqBody.ReqGmail
		data.UserName = ReqBody.ReqUserName
		data.Password = string(hash)
		data.Status = "Active"
		repository := repository.NewUserRepository(db)
		service := service.NewUserService(repository)

		if err := service.RegisterUser(ctx.Request.Context(), &data); err != nil {
			ctx.Redirect(http.StatusMovedPermanently, "/user/register?error=user-registered")
			return
		}

	}
}

func GetLogInForm() func(*gin.Context) {
	return func(ctx *gin.Context) {
		var val string
		err := ctx.Query("error")
		if err == "user-not-exist" {
			val = "User not registered."
		}
		if err == "incorrect-password" {
			val = "Incorrect Password."
		}
		ctx.HTML(http.StatusOK, "LoginForm.html", gin.H{
			"err": val,
		})
	}
}

func LogIn(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data entity.User

		var ReqBody struct {
			ReqGmail    string
			ReqPassword string
		}

		ReqBody.ReqGmail = ctx.PostForm("email")
		ReqBody.ReqPassword = ctx.PostForm("password")

		if err := ctx.ShouldBind(&ReqBody); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		data.Gmail = ReqBody.ReqGmail
		repository := repository.NewUserRepository(db)
		service := service.NewUserService(repository)
		if err := service.UserLogIn(ctx.Request.Context(), &data); err != nil {
			ctx.Redirect(http.StatusMovedPermanently, "/user/login?error=user-not-exist")
			return
		}
		if err := bcrypt.CompareHashAndPassword([]byte(data.Password), []byte(ReqBody.ReqPassword)); err != nil {
			ctx.Redirect(http.StatusMovedPermanently, "/user/login?error=incorrect-password")
			return
		}
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"sub": data.ID,
			"exp": time.Now().Add(time.Hour * 24).Unix(),
		})
		tokenString, err := token.SignedString([]byte(os.Getenv("SECRET_TOKEN")))

		if err != nil {
			ctx.Redirect(http.StatusUnauthorized, "/user/unauthorized?error=token")
			return
		}
		ctx.SetSameSite(http.SameSiteLaxMode)
		ctx.SetCookie("Authorization", tokenString, 3600*24, "", "", false, true)
		ctx.Redirect(http.StatusMovedPermanently, "/")
	}
}

func LogOut() func(*gin.Context) {
	return func(ctx *gin.Context) {
		ctx.SetCookie("Authorization", "", -1, "", "", false, true)
		ctx.Redirect(http.StatusMovedPermanently, "/")
	}
}

func ChangePassword(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data entity.User
		user, _ := ctx.Get("userdata")
		if user == nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		var ReqBody struct {
			ReqOldPassword string
			ReqNewPassword string
		}
		ReqBody.ReqOldPassword = ctx.PostForm("old-password")
		ReqBody.ReqNewPassword = ctx.PostForm("new-password")

		if err := ctx.ShouldBind(&ReqBody); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}

		repository := repository.NewUserRepository(db)
		service := service.NewUserService(repository)

		data.Gmail = user.(entity.User).Gmail
		if err := service.UserLogIn(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": "Could not find such user",
			})
			return
		}
		if err := bcrypt.CompareHashAndPassword([]byte(data.Password), []byte(ReqBody.ReqOldPassword)); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": "Incorrect old password",
			})
			return
		}

		hash, err := bcrypt.GenerateFromPassword([]byte(ReqBody.ReqNewPassword), 10)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		data.Password = string(hash)
		if err := service.ChangePassword(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": "Could not change password.",
			})
		}
		ctx.JSON(http.StatusOK, gin.H{
			"msg": "Password changed",
		})
	}
}

func ResetPassword(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data entity.User
		var ReqBody struct {
			ReqGmail string
		}
		ReqBody.ReqGmail = ctx.PostForm("email")
		repository := repository.NewUserRepository(db)
		service := service.NewUserService(repository)
		if err := service.UserLogIn(ctx.Request.Context(), &data); err != nil {
			ctx.Redirect(http.StatusMovedPermanently, "/user/login?error=user-not-exist")
			return
		}
		newPassword := business.NewPassword(10)
		hash, err := bcrypt.GenerateFromPassword([]byte(newPassword), 10)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		data.Password = string(hash)
		if err := service.ChangePassword(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": "Could not change password.",
			})
		}
		m := gomail.NewMessage()
		// Set E-Mail sender
		m.SetHeader("From", "from-a-reliable-guy@gmail.com")

		// Set E-Mail receivers
		m.SetHeader("To", ReqBody.ReqGmail)

		// Set E-Mail subject
		m.SetHeader("Subject", "Reset")

		// Set E-Mail body. You can set plain text or html with text/html
		m.SetBody("text/plain", "Your new password is now:\n"+newPassword+"\nUse this new password to change your password.")

		// Settings for SMTP server
		d := gomail.NewDialer("smtp.gmail.com", 587, "from-a-reliable-guy@gmail.com", "very-reliable123")

		// This is only needed when SSL/TLS certificate is not valid on server.
		// In production this should be set to false.
		d.TLSConfig = &tls.Config{InsecureSkipVerify: true}

		// Now send E-Mail
		if err := d.DialAndSend(m); err != nil {
			fmt.Println(err)
			panic(err)
		}
		ctx.JSON(http.StatusOK, gin.H{
			"msg": "Please check your email for new password.",
		})
	}
}
