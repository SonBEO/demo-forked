package controller

import (
	"demo/business/service"
	"demo/entity"
	"demo/repository"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func AddProductBookmark(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data entity.BookmarkedProduct
		user, _ := ctx.Get("userdata")
		if user == nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		id, err := strconv.Atoi(ctx.Param("id"))
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		var ReqBody struct {
			ReqUserId    uint
			ReqProductId uint
		}

		ReqBody.ReqUserId = user.(entity.User).ID
		ReqBody.ReqProductId = uint(id)

		if err := ctx.ShouldBind(&ReqBody); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		data.UserId = ReqBody.ReqUserId
		data.ProductId = ReqBody.ReqProductId
		data.Status = "Active"
		repository := repository.NewBookmarkRepository(db)
		service := service.NewBookmarkService(repository)

		if err := service.AddProductBookmark(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"err": "Cannot Add Bookmark",
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"msg": "Bookmark added",
		})
	}
}

func AddShopBookmark(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data entity.BookmarkedShop
		user, _ := ctx.Get("userdata")
		if user == nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		id, err := strconv.Atoi(ctx.Param("id"))
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		var ReqBody struct {
			ReqUserId uint
			ReqShopId uint
		}

		ReqBody.ReqUserId = user.(entity.User).ID
		ReqBody.ReqShopId = uint(id)

		if err := ctx.ShouldBind(&ReqBody); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}

		data.UserId = ReqBody.ReqUserId
		data.ShopId = ReqBody.ReqShopId
		data.Status = "Active"
		repository := repository.NewBookmarkRepository(db)
		service := service.NewBookmarkService(repository)

		if err := service.AddShopBookmark(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"err": "Cannot Add Bookmark",
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"msg": "Bookmark added",
		})
	}
}

func ShowProductBookmark(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data []entity.BookmarkedProduct
		var userdata entity.User
		user, _ := ctx.Get("userdata")
		if user == nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		repository := repository.NewBookmarkRepository(db)
		service := service.NewBookmarkService(repository)
		userdata.ID = user.(entity.User).ID
		if err := service.ShowProductBookmark(ctx.Request.Context(), &data, &userdata); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"err": "Cannot find bookmark",
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"data": data,
		})
	}
}

func ShowShopBookmark(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data []entity.BookmarkedShop
		var userdata entity.User
		user, _ := ctx.Get("userdata")
		if user == nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		repository := repository.NewBookmarkRepository(db)
		service := service.NewBookmarkService(repository)
		userdata.ID = user.(entity.User).ID
		if err := service.ShowShopBookmark(ctx.Request.Context(), &data, &userdata); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"err": "Cannot find bookmark",
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"data": data,
		})
	}
}
func RemoveProductBookmark(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data entity.BookmarkedProduct
		user, _ := ctx.Get("userdata")
		if user == nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		id, err := strconv.Atoi(ctx.Param("id"))
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		var ReqBody struct {
			ReqUserId    uint
			ReqProductId uint
		}
		ReqBody.ReqUserId = user.(entity.User).ID
		ReqBody.ReqProductId = uint(id)
		data.ProductId = ReqBody.ReqProductId
		data.UserId = ReqBody.ReqUserId
		data.Status = "Inactive"
		repository := repository.NewBookmarkRepository(db)
		service := service.NewBookmarkService(repository)
		if err := service.RemoveProductBookmark(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"err": "Cannot remove bookmark",
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"msg": "Bookmarked removed.",
		})
	}
}

func RemoveShopBookmark(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data entity.BookmarkedShop
		user, _ := ctx.Get("userdata")
		if user == nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		id, err := strconv.Atoi(ctx.Param("id"))
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		var ReqBody struct {
			ReqUserId uint
			ReqShopId uint
		}
		ReqBody.ReqUserId = user.(entity.User).ID
		ReqBody.ReqShopId = uint(id)
		data.ShopId = ReqBody.ReqShopId
		data.UserId = ReqBody.ReqUserId
		data.Status = "Inactive"
		repository := repository.NewBookmarkRepository(db)
		service := service.NewBookmarkService(repository)
		if err := service.RemoveShopBookmark(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"err": "Cannot remove bookmark",
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"msg": "Bookmarked removed.",
		})
	}
}
