package controller

import (
	"demo/business/service"
	"demo/entity"
	"demo/repository"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func AccessShop(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data entity.Shop
		user, _ := ctx.Get("userdata")
		if user == nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		repository := repository.NewShopRepository(db)
		service := service.NewShopService(repository)
		data.UserId = user.(entity.User).ID
		if err := service.AccessShop(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": "This user own no shop",
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"ShopOwner":       data.User.UserName,
			"ShopName":        data.ShopName,
			"ShopDescription": data.ShopDescription,
		})
	}
}

func CreateShop(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var data entity.Shop
		user, _ := ctx.Get("userdata")
		if user == nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		repository := repository.NewShopRepository(db)
		service := service.NewShopService(repository)
		var ReqBody struct {
			ReqShopName string
			ReqShopDesc string
		}
		ReqBody.ReqShopDesc = ctx.PostForm("shopdesc")
		ReqBody.ReqShopName = ctx.PostForm("shopname")
		if err := ctx.ShouldBind(&ReqBody); err != nil {
			ctx.JSON(http.StatusUnauthorized, gin.H{
				"error": "Couldn't Bind",
			})
			return
		}
		data.ShopName = ReqBody.ReqShopName
		data.ShopDescription = ReqBody.ReqShopDesc
		data.UserId = user.(entity.User).ID
		data.Status = "Active"
		if err := service.CreateShop(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": "Cannot Create Shop",
				"msg":   err.Error(),
			})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"msg": "New Shop Created",
		})
	}
}
