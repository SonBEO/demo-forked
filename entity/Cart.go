package entity

import "gorm.io/gorm"

type Cart struct {
	gorm.Model
	UserId      uint
	Status      string
	PhoneNumber string
	Location    string
	User        User         `gorm:"foreignKey:UserId"`
	CartDetail  []CartDetail `gorm:"foreignKey:CartId"`
}

func (Cart) TableName() string {
	return "Cart"
}
