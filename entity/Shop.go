package entity

import "gorm.io/gorm"

type Shop struct {
	gorm.Model
	ShopName        string
	UserId          uint `gorm:"unique"`
	ShopDescription string
	Status          string
	User            *User            `gorm:"foreignKey:UserId"`
	Product         []Product        `gorm:"foreignKey:ShopId"`
	BookmarkedShop  []BookmarkedShop `gorm:"foreignKey:ShopId"`
}

func (Shop) TableName() string {
	return "Shop"
}
