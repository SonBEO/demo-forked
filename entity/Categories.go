package entity

import "gorm.io/gorm"

type Categories struct {
	gorm.Model
	CategoryName      string
	Status            string
	ProductCategories []ProductCategories `gorm:"foreignKey:CategoryId"`
}

func (Categories) TableName() string {
	return "Categories"
}
