package entity

import "gorm.io/gorm"

type BookmarkedShop struct {
	gorm.Model
	UserId uint
	ShopId uint
	Status string
	User   User `gorm:"foreignKey:UserId"`
	Shop   Shop `gorm:"foreignKey:ShopId"`
}

func (BookmarkedShop) TableName() string {
	return "BookmarkedShop"
}
