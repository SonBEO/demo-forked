package entity

import "gorm.io/gorm"

type BookmarkedProduct struct {
	gorm.Model
	UserId    uint
	ProductId uint
	Status    string
	User      User    `gorm:"foreignKey:UserId"`
	Product   Product `gorm:"foreignKey:ProductId"`
}

func (BookmarkedProduct) TableName() string {
	return "BookmarkedProduct"
}
