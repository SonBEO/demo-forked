package entity

import "gorm.io/gorm"

type User struct {
	gorm.Model
	Gmail             string `gorm:"unique"`
	UserName          string
	Password          string
	Status            string
	BookmarkedProduct []BookmarkedProduct `gorm:"foreignKey:UserId"`
	BookmarkedShop    []BookmarkedShop    `gorm:"foreignKey:UserId"`
	Shop              *Shop               `gorm:"foreignKey:UserId"`
	Review            []Review            `gorm:"foreignKey:UserId"`
	Cart              []Cart              `gorm:"foreignKey:UserId"`
}

func (User) TableName() string {
	return "User"
}
