package entity

import "gorm.io/gorm"

type ProductCategories struct {
	gorm.Model
	ProductId  uint
	CategoryId uint
	Product    Product    `gorm:"foreignKey:ProductId"`
	Categories Categories `gorm:"foreignKey:CategoryId"`
}

func (ProductCategories) TableName() string {
	return "ProductCategories"
}
