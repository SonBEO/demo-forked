package entity

import "gorm.io/gorm"

type Review struct {
	gorm.Model
	UserId    uint
	ProductId uint
	Rating    int
	Review    string
	Status    string
	User      User    `gorm:"foreignKey:UserId"`
	Product   Product `gorm:"foreignKey:ProductId"`
}

func (Review) TableName() string {
	return "Review"
}
