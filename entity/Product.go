package entity

import "gorm.io/gorm"

type Product struct {
	gorm.Model
	ProductName        string
	ProductDescription string
	Price              float64
	Image              string
	Status             string
	ShopId             uint
	Shop               Shop                `gorm:"foreignKey:ShopId"`
	Review             []Review            `gorm:"foreignKey:ProductId"`
	ProductCategories  []ProductCategories `gorm:"foreignKey:ProductId"`
	CartDetail         []CartDetail        `gorm:"foreignKey:ProductId"`
	BookmarkedProduct  []BookmarkedProduct `gorm:"foreignKey:ProductId"`
}

func (Product) TableName() string {
	return "Product"
}
