package entity

import "gorm.io/gorm"

type CartDetail struct {
	gorm.Model
	CartId    uint
	ProductId uint
	Quantity  int
	Price     float64
	Cart      Cart    `gorm:"foreignKey:CartId"`
	Product   Product `gorm:"foreignKey:ProductId"`
}

func (CartDetail) TableName() string {
	return "CartDetail"
}
