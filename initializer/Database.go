package initializer

import (
	"demo/entity"
	"log"
	"os"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func DBConnect() *gorm.DB {
	dsn := os.Getenv("DB")
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		PrepareStmt: true,
	})
	if err != nil {
		log.Fatalln(err)
	}
	db.AutoMigrate(&entity.User{}, &entity.Shop{}, &entity.Product{},
		&entity.Categories{}, &entity.Cart{}, &entity.Review{},
		&entity.ProductCategories{}, &entity.CartDetail{},
		&entity.BookmarkedProduct{}, &entity.BookmarkedShop{})
	return db
}
